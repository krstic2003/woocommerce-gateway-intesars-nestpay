<?php
/**
 * Plugin Name: WooCommerce Intesa NestPay Gateway
 * Plugin URI: https://www.krstic.in.rs/
 * Description: WooCommerce Intesa NestPay Gateway. 
 * Author: Aleksandar Krstic
 * Author URI: https://www.krstic.in.rs/
 * Version: 1.0.0
 * Text Domain: wc-gateway-intesa
 *
 * License: GNU General Public License v3.0
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 *
 * @package   WC-Gateway-Intesa
 * @author    krstic.in.rs
 * @category  Admin
 * @copyright Copyright (c) 2021, krstic.in.rs
 * @license   http://www.gnu.org/licenses/gpl-3.0.html GNU General Public License v3.0
 *
 */
 
defined( 'ABSPATH' ) or exit;


// Make sure WooCommerce is active
if ( ! in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
	return;
}


/**
 * Add the gateway to WC Available Gateways
 * 
 * @since 1.0.0
 * @param array $gateways all available WC gateways
 * @return array $gateways all WC gateways + offline gateway
 */
function wc_intesa_add_to_gateways( $gateways ) {
	$gateways[] = 'WC_Gateway_Intesa';
	return $gateways;
}
add_filter( 'woocommerce_payment_gateways', 'wc_intesa_add_to_gateways' );


/**
 * Adds plugin page links
 * 
 * @since 1.0.0
 * @param array $links all plugin links
 * @return array $links all plugin links + our custom links (i.e., "Settings")
 */
function wc_intesa_gateway_plugin_links( $links ) {

	$plugin_links = array(
		'<a href="' . admin_url( 'admin.php?page=wc-settings&tab=checkout&section=intesa_gateway' ) . '">' . __( 'Configure', 'wc-gateway-intesa' ) . '</a>'
	);

	return array_merge( $plugin_links, $links );
}
add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), 'wc_intesa_gateway_plugin_links' );


/**
 * Offline Payment Gateway
 *
 * Provides an Offline Payment Gateway; mainly for testing purposes.
 * We load it later to ensure WC is loaded first since we're extending it.
 *
 * @class 		WC_Gateway_Intesa
 * @extends		WC_Payment_Gateway
 * @version		1.0.0
 * @package		WooCommerce/Classes/Payment
 * @author 		SkyVerge
 */
add_action( 'plugins_loaded', 'wc_intesa_gateway_init', 11 );

function wc_intesa_gateway_init() {

	class WC_Gateway_Intesa extends WC_Payment_Gateway {

		/**
		 * Constructor for the gateway.
		 */
		public function __construct() {
	  
			$this->id                 = 'intesa_gateway';
			$this->icon               = apply_filters('woocommerce_offline_icon', '');
			$this->has_fields         = false;
			$this->method_title       = __( 'Intesa NestPay plaćanje platnim karticama.', 'wc-gateway-intesa' );
			$this->method_description = __( 'Plaćanje platnim karticama preko Banka Intese.', 'wc-gateway-intesa' );
		  
			// Load the settings.
			$this->init_form_fields();
			$this->init_settings();
		  
			// Define user set variables
			$this->title        = $this->get_option( 'title' );
			$this->description  = $this->get_option( 'description' );
			$this->instructions = $this->get_option( 'instructions', $this->description );
			$this->url  = $this->get_option( 'url' );
			$this->merchant_id  = $this->get_option( 'merchant_id' );
			$this->store_key  = $this->get_option( 'store_key' );
		  
			// Actions
			add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );
			add_action( 'woocommerce_thankyou_' . $this->id, array( $this, 'thankyou_page' ) );
		  
			// Customer Emails
			add_action( 'woocommerce_email_before_order_table', array( $this, 'email_instructions' ), 10, 3 );
		}
	
	
		/**
		 * Initialize Gateway Settings Form Fields
		 */
		public function init_form_fields() {
	  
			$this->form_fields = apply_filters( 'wc_offline_form_fields', array(
		  
				'enabled' => array(
					'title'   => __( 'Enable/Disable', 'wc-gateway-intesa' ),
					'type'    => 'checkbox',
					'label'   => __( 'Enable Intesa NestPay Payment', 'wc-gateway-intesa' ),
					'default' => 'yes'
				),
				
				'title' => array(
					'title'       => __( 'Title', 'wc-gateway-intesa' ),
					'type'        => 'text',
					'description' => __( 'This controls the title for the payment method the customer sees during checkout.', 'wc-gateway-intesa' ),
					'default'     => __( 'Intesa NestPay Payment', 'wc-gateway-intesa' ),
					'desc_tip'    => true,
				),
				
				'description' => array(
					'title'       => __( 'Description', 'wc-gateway-intesa' ),
					'type'        => 'textarea',
					'description' => __( 'Payment method description that the customer will see on your checkout.', 'wc-gateway-intesa' ),
					'desc_tip'    => true,
				),

				'url' => array(
					'title'       => __( 'Gateway URL', 'wc-gateway-intesa' ),
					'type'        => 'text',
					'description' => __( 'Gateway URL.', 'wc-gateway-intesa' ),
					'default'     => 'https://testsecurepay.eway2pay.com/fim/est3Dgate',
					'desc_tip'    => true,
				),

				'merchant_id' => array(
					'title'       => __( 'Merchant ID', 'wc-gateway-intesa' ),
					'type'        => 'text',
					'desc_tip'    => true,
				),

				'store_key' => array(
					'title'       => __( 'Store Key', 'wc-gateway-intesa' ),
					'type'        => 'text',
					'desc_tip'    => true,
				),
				
				'instructions' => array(
					'title'       => __( 'Instructions', 'wc-gateway-intesa' ),
					'type'        => 'textarea',
					'description' => __( 'Instructions that will be added to the thank you page and emails.', 'wc-gateway-intesa' ),
					'desc_tip'    => true,
				),
			) );
		}
	
	
		/**
		 * Output for the order received page.
		 */
		public function thankyou_page($order_id) {
				$order = wc_get_order( $order_id );
				$ok_url = get_site_url() . "/checkout/order-received/".$order_id."/?key=".$order->get_order_key();
				$rnd = rand();
				$order_hash_plain = $this->merchant_id.'|'.$order_id.'|'.$order->get_total().'|'.$ok_url.'|'.$ok_url.'|PreAuth||'.$rnd.'||||941|'.$this->store_key;
				$order_hash = base64_encode(pack('H*', hash('sha512', $order_hash_plain)));

			    

                if (isset($_REQUEST['ReturnOid'])) {
                	echo '<div style="text-align:center;font-weight:bold;">';
                	echo '<h1 style="text-align:center">USPEŠNO STE PLATILI VAŠU PORUDŽBINU</h1>';
                	echo '<h3 style="text-align:center">Detalji:</h3>';
                	echo 'Broj kartice: ' . $_REQUEST['maskedCreditCard'] . '<br>';
                	echo 'Naplaćen iznos: ' . $_REQUEST['amount'] . '<br>';
                	echo 'Datum: ' . $_REQUEST['EXTRA_TRXDATE'] . '<br>';
                	echo 'Broj transkacije: ' . $_REQUEST['TransId'] . '<br>';
                	echo '</div>';
                	$order->update_status( 'processing', __( 'Plaćeno karticom klijenta', 'wc-gateway-intesa' ) );
                }else{
                	echo "<form id='intesa-cc' style='text-align:center;' action='".$this->url."' method='POST'>";
	                echo "<input type='hidden' name='failUrl' value='".$ok_url."' />";
	                echo "<input type='hidden' name='currency' value='941' />";
	                echo "<input type='hidden' name='trantype' value='PreAuth' />";
	                echo "<input type='hidden' name='okUrl' value='".$ok_url."' />";
	                echo "<input type='hidden' name='amount' value='".$order->get_total()."' />";
	                echo "<input type='hidden' name='oid' value='".$order->get_id()."' />";
	                echo "<input type='hidden' name='clientid' value='".$this->merchant_id."' />";
	                echo "<input type='hidden' name='storetype' value='3d_pay_hosting' />";
	                echo "<input type='hidden' name='lang' value='sr' />";
	                echo "<input type='hidden' name='hashAlgorithm' value='ver2' />";
	                echo "<input type='hidden' name='rnd' value='".$rnd."' />";
	                echo "<input type='hidden' name='encoding' value='utf-8' />";
	                echo "<input type='hidden' name='hash' value='".$order_hash."' />";
	                echo "<input type='submit' value='Plati platnom karticom'>";
	                echo "</form>";
                }
		}
	
	
		/**
		 * Add content to the WC emails.
		 *
		 * @access public
		 * @param WC_Order $order
		 * @param bool $sent_to_admin
		 * @param bool $plain_text
		 */
		public function email_instructions( $order, $sent_to_admin, $plain_text = false ) {
		
			if ( $this->id === $order->get_payment_method() ) {
				$order_id = $order->get_id();
				$ok_url = get_site_url() . "/checkout/order-received/".$order_id."/?key=".$order->get_order_key();
				$rnd = rand();
				$order_hash_plain = $this->merchant_id.'|'.$order_id.'|'.$order->get_total().'|'.$ok_url.'|'.$ok_url.'|PreAuth||'.$rnd.'||||941|'.$this->store_key;
				$order_hash = base64_encode(pack('H*', hash('sha512', $order_hash_plain)));

			    

                if (isset($_REQUEST['ReturnOid'])) {
                	echo '<div style="text-align:center;font-weight:bold;">';
                	echo '<h1 style="text-align:center">USPEŠNO STE PLATILI VAŠU PORUDŽBINU</h1>';
                	echo '<h3 style="text-align:center">Detalji:</h3>';
                	echo 'Broj kartice: ' . $_REQUEST['maskedCreditCard'] . '<br>';
                	echo 'Naplaćen iznos: ' . $_REQUEST['amount'] . '<br>';
                	echo 'Datum: ' . $_REQUEST['EXTRA_TRXDATE'] . '<br>';
                	echo 'Broj transkacije: ' . $_REQUEST['TransId'] . '<br>';
                	echo '</div>';
                	$order->update_status( 'processing', __( 'Plaćeno karticom klijenta', 'wc-gateway-intesa' ) );
                }else{
                	echo "<form id='intesa-cc' style='text-align:center;' action='".$this->url."' method='POST'>";
	                echo "<input type='hidden' name='failUrl' value='".$ok_url."' />";
	                echo "<input type='hidden' name='currency' value='941' />";
	                echo "<input type='hidden' name='trantype' value='PreAuth' />";
	                echo "<input type='hidden' name='okUrl' value='".$ok_url."' />";
	                echo "<input type='hidden' name='amount' value='".$order->get_total()."' />";
	                echo "<input type='hidden' name='oid' value='".$order->get_id()."' />";
	                echo "<input type='hidden' name='clientid' value='".$this->merchant_id."' />";
	                echo "<input type='hidden' name='storetype' value='3d_pay_hosting' />";
	                echo "<input type='hidden' name='lang' value='sr' />";
	                echo "<input type='hidden' name='hashAlgorithm' value='ver2' />";
	                echo "<input type='hidden' name='rnd' value='".$rnd."' />";
	                echo "<input type='hidden' name='encoding' value='utf-8' />";
	                echo "<input type='hidden' name='hash' value='".$order_hash."' />";
	                echo "<input type='submit' value='Plati platnom karticom'>";
	                echo "</form>";
                }
			}
		}
	
	
		/**
		 * Process the payment and return the result
		 *
		 * @param int $order_id
		 * @return array
		 */
		public function process_payment( $order_id ) {

			$order = wc_get_order( $order_id );
			
			// Mark as on-hold (we're awaiting the payment)
			$order->update_status( 'pending', __( 'Čeka se plaćanje', 'wc-gateway-intesa' ) );

			$ok_url = get_site_url() . "/checkout/order-received/".$order_id."/?key=".$order->get_order_key();
			$order->add_order_note( 'Link za plaćanje: ' . $ok_url );
			
			// Reduce stock levels
			$order->reduce_order_stock();
			
			// Remove cart
			WC()->cart->empty_cart();
			
			// Return thankyou redirect
			return array(
				'result' 	=> 'success',
				'redirect'	=> $this->get_return_url( $order )
			);
		}
	
  } // end \WC_Gateway_Intesa class
}