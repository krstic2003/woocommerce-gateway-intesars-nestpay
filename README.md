# Woo Intesa Serbia CC payment gateway

WordPress/WooCommerce payment method plugin for Banca Intesa "NestPay" - Works in Serbia 

Installation:

1. Upload the entire 'woocommerce-gateway-intesars-nestpay' folder to the '/wp-content/plugins/' directory.
2. Activate the plugin through the 'Plugin' menu in WordPress.

Configuration:

1. Go to YOUR-WP-SITE/wp-admin/admin.php?page=wc-settings&tab=checkout&section=intesa_gateway

Requirements:

1. Woocommerce plugin installed and activated.